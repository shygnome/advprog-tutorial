package applicant;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicantTest {

    private Applicant applicant;
    private Class<?> applicantClass;

    @Before
    public void setUp() throws Exception {
        applicant = new Applicant();
        applicantClass = Class.forName("applicant.Applicant");
    }

    @Test
    public void testMethodIsCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void testGetCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testGetEmploymentYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testHasCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testEvaluate() {
        Predicate<Applicant> qualifiedEvaluator =
                QualifiedEvaluator.QUALIFIED_EVALUATOR;

        Predicate<Applicant> creditEvaluator =
                CreditEvaluator.CREDIT_EVALUATOR;

        Predicate<Applicant> employmentEvaluator =
                EmploymentEvaluator.EMPLOYMENT_EVALUATOR;

        Predicate<Applicant> crimeCheck =
                CriminalRecordsEvaluator.CRIME_CHECK;

        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator).and((crimeCheck))));
    }
}
