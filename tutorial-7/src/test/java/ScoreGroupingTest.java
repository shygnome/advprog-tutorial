import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreGroupingTest {

    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();
    }

    @Test
    public void testGroupingWithDistinctScores() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 13);
        scores.put("Emi", 14);
        scores.put("Foxtrot", 16);

        assertEquals(6, ScoreGrouping.groupByScores(scores).size());
        assertEquals(1, ScoreGrouping.groupByScores(scores).get(15).size());
    }

    @Test
    public void testGroupingWithDuplicateScores() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        assertEquals(3, ScoreGrouping.groupByScores(scores).size());
        assertEquals(3, ScoreGrouping.groupByScores(scores).get(15).size());
    }

}