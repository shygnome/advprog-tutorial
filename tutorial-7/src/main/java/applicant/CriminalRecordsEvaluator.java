package applicant;

import java.util.function.Predicate;

public class CriminalRecordsEvaluator {

    public static final Predicate<Applicant> CRIME_CHECK =
            anApplicant -> !anApplicant.hasCriminalRecord();

}
