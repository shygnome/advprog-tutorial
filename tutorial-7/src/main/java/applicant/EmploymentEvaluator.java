package applicant;

import java.util.function.Predicate;

public class EmploymentEvaluator {

    public static final Predicate<Applicant> EMPLOYMENT_EVALUATOR =
            anApplicant -> anApplicant.getEmploymentYears() > 0;

}
