package applicant;

import java.util.function.Predicate;

public class QualifiedEvaluator {

    public static final Predicate<Applicant> QUALIFIED_EVALUATOR =
            Applicant::isCredible;

}
