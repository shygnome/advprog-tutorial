package applicant;

import java.util.function.Predicate;

public class CreditEvaluator {

    public static final Predicate<Applicant> CREDIT_EVALUATOR =
            anApplicant -> anApplicant.getCreditScore() > 600;
}