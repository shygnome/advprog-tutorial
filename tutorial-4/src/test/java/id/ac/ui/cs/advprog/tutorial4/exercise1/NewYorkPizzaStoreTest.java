package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Yudistira on 3/9/2018.
 */
public class NewYorkPizzaStoreTest {

    private PizzaStore nyStore;

    @Before
    public void setUp() throws Exception {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza = nyStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());

        pizza = nyStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());

        pizza = nyStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }
}
