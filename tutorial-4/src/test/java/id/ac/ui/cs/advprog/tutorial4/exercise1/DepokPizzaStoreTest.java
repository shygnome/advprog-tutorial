package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Yudistira on 3/9/2018.
 */
public class DepokPizzaStoreTest {

    private PizzaStore dpStore;

    @Before
    public void setUp() throws Exception {
        dpStore = new DepokPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza = dpStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());

        pizza = dpStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());

        pizza = dpStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
