package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by Yudistira on 3/8/2018.
 */
public class SalsaSauce implements Sauce {
    public String toString() {
        return "Salsa Sauce";
    }
}
