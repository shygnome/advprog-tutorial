package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by Yudistira on 3/8/2018.
 */
public class YellowPepper implements Veggies {

    public String toString() {
        return "Yellow Pepper";
    }
}
