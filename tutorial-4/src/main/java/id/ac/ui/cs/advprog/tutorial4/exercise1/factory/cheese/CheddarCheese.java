package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created by Yudistira on 3/8/2018.
 */
public class CheddarCheese implements Cheese {

    public String toString() {
        return "Shredded Cheddar";
    }
}
