package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * Created by Yudistira on 3/8/2018.
 */
public class SteamedClams implements Clams {

    public String toString() {
        return "Steamed Clams from Bikini Bottom";
    }
}
