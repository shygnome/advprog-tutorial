package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * Created by Yudistira on 3/8/2018.
 */
public class RegularDough implements Dough {
    public String toString() {
        return "Regular Style Dough";
    }
}
