import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<p>Amount owed is <em>3.5</em></p>"));
        assertTrue(result.contains("<em>1</em> frequent renter points</p>"));
    }

    @Test
    public void statementWithMultipleMovies() {
        Movie dummy1 = new Movie("Pink Panther", Movie.CHILDREN);
        Movie dummy2 = new Movie("Black Panther", Movie.NEW_RELEASE);
        Rental rent1 = new Rental(dummy1, 4);
        Rental rent2 = new Rental(dummy2, 3);

        customer.addRental(rent);
        customer.addRental(rent1);
        customer.addRental(rent2);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 15.5"));
        assertTrue(result.contains("4 frequent renter points"));
    }
}