package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by Yudistira on 2/28/2018.
 */
public class Cucumber extends Filling {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        return food.cost() + 0.40;
    }
}
