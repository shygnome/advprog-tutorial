package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by Yudistira on 2/28/2018.
 */
public class Lettuce extends Filling {
    Food food;

    public Lettuce(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding lettuce";
    }

    @Override
    public double cost() {
        return food.cost() + 0.75;
    }
}
