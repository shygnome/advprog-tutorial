package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
    }

    public double getNetSalaries() {
        return employeesList.stream().collect(Collectors.summingDouble(i -> i.getSalary()));
    }

    public List<Employees> getAllEmployees() {
        return employeesList;
    }
}
