package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by Yudistira on 3/1/2018.
 */
public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        if (salary  < 50000.00) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
