package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

/**
 * Created by Yudistira on 3/2/2018.
 */
public class CompositeDemo {
    public static void main(String[] args) {
        Company company = new Company();

        Ceo dummy1 = new Ceo("Luffy", 500000.00);
        company.addEmployee(dummy1);

        Cto dummy2 = new Cto("Zorro", 320000.00);
        company.addEmployee(dummy2);

        BackendProgrammer dummy3 = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(dummy3);

        FrontendProgrammer dummy4 = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(dummy4);

        UiUxDesigner dummy5 = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(dummy5);

        NetworkExpert dummy6 = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(dummy6);

        SecurityExpert dummy7 = new SecurityExpert("Chopper", 80000.00);
        company.addEmployee(dummy7);

        List<Employees> allEmployees = company.getAllEmployees();
        allEmployees.stream().forEach(i -> System.out.println(i.getRole() + " "
                + i.getName() + " dengan gaji " + i.getSalary()));
        System.out.println("Gaji total " + company.getNetSalaries());
    }
}
