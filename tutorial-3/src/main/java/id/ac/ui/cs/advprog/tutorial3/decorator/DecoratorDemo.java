package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

/**
 * Created by Yudistira on 3/2/2018.
 */
public class DecoratorDemo {
    public static void main(String[] args) {
        Food thickBunBurgerSpecial;

        //Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost() + " "
                + thickBunBurgerSpecial.getDescription());
        System.out.println();

        Food thinBunBurgerVegetarian;

        //Thin Bun Burger with Tomato, Lettuce, Cucumber
        thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        System.out.println(thinBunBurgerVegetarian.cost() + " "
                + thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.cost() + " "
                + thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.cost() + " "
                + thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.cost() + " "
                + thinBunBurgerVegetarian.getDescription());
        System.out.println();

        Food doubleBeefChickenDoubleSauceSandwich;

        //Crusty Sandiwich with Beef Meat, Chicken Meat, Using Tomato and Chili Sauce
        doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        System.out.println(doubleBeefChickenDoubleSauceSandwich.cost() + " "
                + doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.cost() + " "
                + doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.cost() + " "
                + doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.cost() + " "
                + doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.cost() + " "
                + doubleBeefChickenDoubleSauceSandwich.getDescription());
        System.out.println();

        Food noCrustAllFillingSandwich;

        //No Crust Sandiwich with All Filling
        noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();

        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.cost() + " "
                + noCrustAllFillingSandwich.getDescription());
    }
}
